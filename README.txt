CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

In the help of simplenews letter advance plugins user can manage,
our Newsletter subscription on account configure time and user 
registration page.


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

 * Simplenews (https://www.drupal.org/project/simplenews):
   Simplenews publishes and sends newsletters to lists of subscribers.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Raushan Tiwari - https://www.drupal.org/u/raushan
